class User < ApplicationRecord
  has_many :microposts
  validates :name , length: { minimum: 4 },
                      presence: true
  validates :email, length: { maximum: 14 },
                      presence: true
end
